Author: Helmut Grohne <helmut@subdivi.de>
Description: do not use AC_RUN_IFELSE
 povray fails to cross build from source, because it uses AC_RUN_IFELSE
 in a few places. Getting rid of these isn't obvious. It's used for
 different things:
 * Checking whether boost thread works. -> AC_LINK_IFELSE suffices here.
 * Determining versions of libraries. -> AC_COMPUTE_INT often works.
 * Determining version of libtiff. -> We can opportunistically use
   PKG_CHECK_MODULES and fall back to the old check on failure.
Bug-Debian: https://bugs.debian.org/946477
Bug: https://github.com/POV-Ray/povray/issues/388

--- povray-3.7.0.8.orig/unix/configure.ac
+++ povray-3.7.0.8/unix/configure.ac
@@ -60,7 +60,6 @@
 m4_include([unix/config/ax_boost_base.m4])
 m4_include([unix/config/ax_boost_thread.m4])
 m4_include([unix/config/ax_test_compiler_flags.m4])
-m4_include([unix/config/ax_check_lib.m4])
 m4_include([unix/config/ax_check_libjpeg.m4])
 m4_include([unix/config/ax_check_libsdl.m4])
 m4_include([unix/config/ax_check_libtiff.m4])
@@ -314,7 +313,7 @@
 do
   LIBS=$SAVED_LIBS
   LIBS="$LIBS $extralib"
-  AC_RUN_IFELSE([
+  AC_LINK_IFELSE([
     AC_LANG_PROGRAM([[
       #include <boost/thread/thread.hpp>
     ]],[[
@@ -326,9 +325,7 @@
     AC_MSG_RESULT([yes])
     BOOST_THREAD_LIB="$BOOST_THREAD_LIB $extralib"
     boost_thread_links=1
-  ],,[
-    AC_MSG_RESULT([cross-compiling])  # FIXME
-  ])
+  ],)
   if test $boost_thread_links = '1'; then
     break
   fi
@@ -363,7 +360,20 @@
     AC_MSG_ERROR([disabling support for ZLIB requires NON_REDISTRIBUTABLE_BUILD=yes])
   fi
   AC_MSG_RESULT([yes])
-  AX_CHECK_LIB([z], [$required_libz_version], [z], [zlibVersion], [zlib.h], [zlibVersion()], [$with_zlib])
+  if test x"$with_zlib" != x; then
+    CPPFLAGS="-I$with_zlib/../include $CPPFLAGS"
+    LDFLAGS="-L$with_zlib $LDFLAGS"
+  fi
+  AC_SEARCH_LIBS([deflate],[z],[
+    AC_CHECK_HEADER([zlib.h],[
+      AC_MSG_CHECKING([for libz version >= $required_libz_version])
+      AC_COMPUTE_INT([ZLIB_VER_MAJOR],[ZLIB_VER_MAJOR],[#include <zlib.h>])
+      AC_COMPUTE_INT([ZLIB_VER_MINOR],[ZLIB_VER_MINOR],[#include <zlib.h>])
+      AC_COMPUTE_INT([ZLIB_VER_REVISION],[ZLIB_VER_REVISION],[#include <zlib.h>])
+      AX_COMPARE_VERSION([$ZLIB_VER_MAJOR.$ZLIB_VER_MINOR.$ZLIB_VER_REVISION],[ge],[$required_libz_version],[ax_check_lib=ok],[ax_check_lib=bad])
+      AC_MSG_RESULT([$ZLIB_VER_MAJOR.$ZLIB_VER_MINOR.$ZLIB_VER_REVISION, $ax_check_lib])
+    ],[ax_check_lib="no headers"])
+  ],[ax_check_lib="not found"])
   if test x"$ax_check_lib" != x"ok"; then
     AC_MSG_ERROR([cannot find a suitable ZLIB library])
   else
@@ -382,7 +392,20 @@
     AC_MSG_ERROR([disabling support for PNG requires NON_REDISTRIBUTABLE_BUILD=yes])
   fi
   AC_MSG_RESULT([yes])
-  AX_CHECK_LIB([png], [$required_libpng_version], [png14 png png12 png], [png_get_libpng_ver], [png.h], [png_get_libpng_ver(NULL)], [$with_libpng])
+  if test x"$with_libpng" != x; then
+    CPPFLAGS="-I$with_libpng/../include $CPPFLAGS"
+    LDFLAGS="-L$with_libpng $LDFLAGS"
+  fi
+  AC_SEARCH_LIBS([png_get_libpng_ver],[png14 png png12 png],[
+    AC_CHECK_HEADER([png.h],[
+      AC_MSG_CHECKING([for libpng version >= $required_libpng_version])
+      AC_COMPUTE_INT([PNG_LIBPNG_VER_MAJOR],[PNG_LIBPNG_VER_MAJOR],[#include <png.h>])
+      AC_COMPUTE_INT([PNG_LIBPNG_VER_MINOR],[PNG_LIBPNG_VER_MINOR],[#include <png.h>])
+      AC_COMPUTE_INT([PNG_LIBPNG_VER_RELEASE],[PNG_LIBPNG_VER_RELEASE],[#include <png.h>])
+      AX_COMPARE_VERSION([$PNG_LIBPNG_VER_MAJOR.$PNG_LIBPNG_VER_MINOR.$PNG_LIBPNG_VER_RELEASE],[ge],[$required_libpng_version],[ax_check_lib=ok],[$ax_check_lib=bad])
+      AC_MSG_RESULT([$PNG_LIBPNG_VER_MAJOR.$PNG_LIBPNG_VER_MINOR.$PNG_LIBPNG_VER_RELEASE, $ax_check_lib])
+    ],[ax_check_lib="no headers"])
+  ],[ax_check_lib="not found"])
   ### FIXME: do not allow for 1.4.x
 	# This doesn't appear to be needed now that we're allowing 1.4 (jh)
   # AC_MSG_CHECKING([for libpng version < 1.4 (not supported at the moment!)])
--- povray-3.7.0.8.orig/unix/config/ax_check_lib.m4
+++ /dev/null
@@ -1,79 +0,0 @@
-# SYNOPSIS
-#
-#   AX_CHECK_LIB(lib, required_version, search_libs, check_function, header, version_function, lib_dir)
-#
-# DESCRIPTION
-#
-#   Check whether a function is found in a set of libraries and compare
-#   the library version to the required one.
-#
-# LAST MODIFICATION
-#
-#   2007-11-08
-#
-# COPYLEFT
-#
-#   Copyright (c) 2006 Nicolas Calimet
-#
-#   Copying and distribution of this file, with or without
-#   modification, are permitted in any medium without royalty provided
-#   the copyright notice and this notice are preserved.
-
-AC_DEFUN([AX_CHECK_LIB],
-[
-  ax_check_lib_save_cppflags="$CPPFLAGS"
-  ax_check_lib_save_ldflags="$LDFLAGS"
-  if test x"$7" != x""; then
-    CPPFLAGS="-I$7/../include $CPPFLAGS"
-    LDFLAGS="-L$7 $LDFLAGS"
-  fi
-
-  # check the library
-  AC_SEARCH_LIBS(
-    [$4],
-    [$3],
-    [
-      # check include file
-      AC_CHECK_HEADER(
-        [$5],
-        [
-          # check library version, update LIBS
-          AC_MSG_CHECKING([for lib$1 version >= $2])
-          AC_RUN_IFELSE(
-            [
-              AC_LANG_SOURCE(
-[#include <stdio.h>
-#include <string.h>
-#include "$5"
-int main (void)
-{
-  const char *version = $6;
-  fprintf (stderr, "%s\n", version);
-  return 0;
-}]
-              )
-            ],
-            [
-              ax_check_lib_version=`eval $ac_try 2>&1`
-              AX_COMPARE_VERSION([$ax_check_lib_version], [ge], [$2], [ax_check_lib="ok"], [ax_check_lib="bad"])
-              AC_MSG_RESULT([$ax_check_lib_version, $ax_check_lib])
-            ],
-            [
-              ax_check_lib="unknown"
-              AC_MSG_RESULT([$ax_check_lib])
-            ],
-            [AC_MSG_RESULT([cross-compiling, forced])]
-          )  # AC_RUN_IFELSE
-        ],
-        [ax_check_lib="no headers"]
-      )  # AC_CHECK_HEADER
-    ],
-    [ax_check_lib="not found"],
-    []
-  )  # AC_SEARCH_LIBS
-
-  if test x"$ax_check_lib" != x"ok"; then
-    CPPFLAGS="$ax_check_lib_save_cppflags"
-    LDFLAGS="$ax_check_lib_save_ldflags"
-  fi
-])
--- povray-3.7.0.8.orig/unix/config/ax_check_libjpeg.m4
+++ povray-3.7.0.8/unix/config/ax_check_libjpeg.m4
@@ -45,29 +45,9 @@
           else
             AC_MSG_CHECKING([for libjpeg version >= $1])
           fi
-          AC_RUN_IFELSE(
-            [
-              AC_LANG_SOURCE(
-[#include <stdio.h>
-#include "jpeglib.h"
-int main (void)
-{
-  fprintf (stderr, "%d\n", JPEG_LIB_VERSION);
-  return 0;
-}]
-              )
-            ],
-            [
-              ax_check_libjpeg_version=`eval $ac_try 2>&1`
-              AX_COMPARE_VERSION([$ax_check_libjpeg_version], [ge], [$ax_check_libjpeg_version_num], [ax_check_libjpeg="ok"], [ax_check_libjpeg="bad"])
-              AC_MSG_RESULT([$ax_check_libjpeg_version, $ax_check_libjpeg])
-            ],
-            [
-              ax_check_libjpeg="unknown"
-              AC_MSG_RESULT([$ax_check_libjpeg])
-            ],
-            [AC_MSG_RESULT([cross-compiling, forced])]
-          )  # AC_RUN_IFELSE
+          AC_COMPUTE_INT([JPEG_LIB_VERSION],[JPEG_LIB_VERSION],[#include "jpeglib.h"])
+          AX_COMPARE_VERSION([$ax_check_libjpeg_version], [ge], [$JPEG_LIB_VERSION], [ax_check_libjpeg="ok"], [ax_check_libjpeg="bad"])
+          AC_MSG_RESULT([$JPEG_LIB_VERSION, $ax_check_libjpeg])
         ],
         [ax_check_libjpeg="no headers"]
       )  # AC_CHECK_HEADER
--- povray-3.7.0.8.orig/unix/config/ax_check_libtiff.m4
+++ povray-3.7.0.8/unix/config/ax_check_libtiff.m4
@@ -20,6 +20,11 @@
 
 AC_DEFUN([AX_CHECK_LIBTIFF],
 [
+  PKG_CHECK_MODULES([LIBTIFF],[libtiff-4 >= $1],[
+    CPPFLAGS="$LIBTIFF_CFLAGS $CPPFLAGS"
+    LIBS="$LIBTIFF_LIBS $LIBS"
+    ax_check_libtiff=ok
+  ],[
   ax_check_libtiff_save_cppflags="$CPPFLAGS"
   ax_check_libtiff_save_ldflags="$LDFLAGS"
   if test x"$2" != x""; then
@@ -84,4 +89,5 @@
     CPPFLAGS="$ax_check_libtiff_save_cppflags"
     LDFLAGS="$ax_check_libtiff_save_ldflags"
   fi
+  ])
 ])
