#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/architecture.mk

ifeq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CXXFLAGS += -O3
	
	# This is rumoured to give wrong results with zero-thickness boxes, so
	# disabled
	# CXXFLAGS += -ffast-math
		
	ifeq ($(DEB_HOST_ARCH),i386)
		 CXXFLAGS += -minline-all-stringops -malign-double
	endif
endif


%:
	dh $@

override_dh_auto_configure:
	set -e ; cd unix/config && for ax in *.m4 ; do if [ ! -h "$$ax" ] && [ -f "/usr/share/aclocal/$$ax" ]; then mv -v "$$ax" "$$ax.dist" ; ln -sv "/usr/share/aclocal/$$ax" ; fi ; done
	if [ -d libraries ] && [ ! -d libraries.donotuse ] ; then mv -v libraries libraries.donotuse ; fi
	cd unix && ./prebuild.sh
	dh_auto_configure -- \
		--disable-optimiz \
		--disable-strip \
		--with-boost-thread=boost_thread \
		COMPILED_BY="$(shell dpkg-parsechangelog | sed -n '/^Maintainer/{s/Maintainer: //;s/@debian.org/ at debian dot org/;p}') for Debian <www.debian.org>" \
		CXXFLAGS="$(CXXFLAGS)" \
		LDFLAGS="$(LDFLAGS)"

execute_after_dh_auto_clean:
	-cd unix && ./prebuild.sh clean
	$(RM) install.log
	$(RM) unix/config/compile
	if [ -d libraries.donotuse ] && [ ! -d libraries ] ; then mv -v libraries.donotuse libraries ; fi
	-test ! -d .git || git checkout libraries
	set -e ; cd unix/config && for ax in *.m4.dist ; do if [ -f "$$ax" ] ; then mv -v "$$ax" "$${ax%.dist}" ; fi ; done

override_dh_installchangelogs:
	dh_installchangelogs changes.txt

override_dh_compress:
	dh_compress -X usr/share/doc/povray/examples
